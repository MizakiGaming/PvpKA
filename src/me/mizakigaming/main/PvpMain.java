package me.mizakigaming.main;

import org.bukkit.plugin.java.JavaPlugin;

import me.mizakigaming.pvp.commands.PvpEventActivate;
import me.mizakigaming.pvp.commands.PvpInfo;
import me.mizakigaming.pvp.commands.ViewBestScore;
import me.mizakigaming.pvp.events.DeathEvent;
import me.mizakigaming.pvp.events.JoinEvent;

public class PvpMain extends JavaPlugin
{

	private static PvpMain		instance;
	private PvpEventActivate	command;
	private JoinEvent			join;
	private DeathEvent			death;
	private ViewBestScore		bestScore;
	private PvpInfo info;

	@Override
	public void onEnable()
	{
		saveDefaultConfig();
		if (!this.getConfig().contains("BestScore"))
		{
			this.getConfig().set("BestScore.Score", 0);
			this.getConfig().set("BestScore.Joueur", "null");
		}

		this.saveConfig();

		instance = this;
		this.info = new PvpInfo();
		this.command = new PvpEventActivate();
		this.join = new JoinEvent();
		this.death = new DeathEvent();
		this.bestScore = new ViewBestScore();

		this.getCommand("pvpinfo").setExecutor(this.info);
		this.getCommand("mcpvp").setExecutor(this.command);
		this.getCommand("score").setExecutor(this.bestScore);
		this.getServer().getPluginManager().registerEvents(this.join, this);
		this.getServer().getPluginManager().registerEvents(this.death, this);
		
	}

	@Override
	public void onDisable()
	{

	}

	public static PvpMain getInstance()
	{
		return instance;
	}

	public PvpEventActivate getCommand()
	{
		return command;
	}

	public JoinEvent getJoin()
	{
		return join;
	}

	public DeathEvent getDeath()
	{
		return death;
	}
}
