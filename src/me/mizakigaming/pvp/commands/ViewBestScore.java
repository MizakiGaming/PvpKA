package me.mizakigaming.pvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import me.mizakigaming.main.PvpMain;
import net.md_5.bungee.api.ChatColor;

public class ViewBestScore implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("score"))
		{
			if (sender instanceof Player)
			{
				Player p = (Player) sender;
				p.sendMessage(ChatColor.GRAY + "Meilleur Killstreak:");
				p.sendMessage("- " + ChatColor.GOLD + PvpMain.getInstance().getConfig().getString("BestScore.Joueur"));
				p.sendMessage("- " + ChatColor.GOLD + PvpMain.getInstance().getConfig().getInt("BestScore.Score"));
				return true;
			}
			else if (sender instanceof ConsoleCommandSender)
			{
				Bukkit.getConsoleSender().sendMessage(ChatColor.GRAY + "Meilleur Killstreak:");
				Bukkit.getConsoleSender().sendMessage(
						"- " + ChatColor.GOLD + PvpMain.getInstance().getConfig().getString("BestScore.Joueur"));
				Bukkit.getConsoleSender().sendMessage(
						"- " + ChatColor.GOLD + PvpMain.getInstance().getConfig().getString("BestScore.Score"));
				return true;
			}
			else
				return false;
		}
		return false;
	}

}
