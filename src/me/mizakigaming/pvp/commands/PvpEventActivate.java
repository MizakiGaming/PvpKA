package me.mizakigaming.pvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class PvpEventActivate implements CommandExecutor
{

	private boolean Start = true;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (cmd.getName().equalsIgnoreCase("mcpvp"))
		{
			if (sender instanceof Player)
			{
				Player p = (Player) sender;
				if (this.Start == true)
				{
					this.Start = false;
					p.sendMessage("pvpeffect disable");
				}
				else
				{
					this.Start = true;
					p.sendMessage("pvpeffect enable");
				}
			}
			else if (sender instanceof ConsoleCommandSender)
			{
				if (this.Start == true)
				{
					this.Start = false;
					Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "pvpeffect disable");
				}
				else
				{
					this.Start = true;
					Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "pvpeffect enable");
				}
			}
			else
				return false;
		}
		else
			return false;
		return false;
	}

	public boolean isStart()
	{
		return Start;
	}

	public void setStart(boolean start)
	{
		Start = start;
	}

}
