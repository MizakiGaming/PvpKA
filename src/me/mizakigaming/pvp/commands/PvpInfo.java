package me.mizakigaming.pvp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class PvpInfo implements CommandExecutor
{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		
		if(cmd.getName().equalsIgnoreCase("pvpinfo"))
		{
			if(sender instanceof Player)
			{
				this.liste((Player) sender);
				return true;
			}
			else if(sender instanceof ConsoleCommandSender)
			{
				return false;
			}
			else return false;
		}
		return false;
	}
	
	public void liste(Player player)
	{
		player.sendMessage(ChatColor.GRAY + "--------------------------------");
		player.sendMessage(ChatColor.GRAY + "Liste des effets par killstreak:");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 3:");
		player.sendMessage(ChatColor.GOLD + "- Speed I 20 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 5:");
		player.sendMessage(ChatColor.GOLD + "- Récupération de toute ta vie");
		player.sendMessage(ChatColor.GOLD + "- Absorption I 20 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 7:");
		player.sendMessage(ChatColor.GOLD + "- Speed II 40 secondes");
		player.sendMessage(ChatColor.GOLD + "- Fire_Resistance II 40 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 10:");
		player.sendMessage(ChatColor.GOLD + "- Absorption II 25 secondes");
		player.sendMessage(ChatColor.GOLD + "- Damage_Resistance II 25 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 15:");
		player.sendMessage(ChatColor.GOLD + "- Speed II 60 secondes");
		player.sendMessage(ChatColor.GOLD + "- Health_Boost II 40 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 20:");
		player.sendMessage(ChatColor.GOLD + "- Speed II 120 secondes");
		player.sendMessage(ChatColor.GOLD + "- Damage_Resistance II 120 secondes");
		player.sendMessage(ChatColor.GOLD + "- Health_Boost II 60 secondes");
		player.sendMessage(ChatColor.GRAY + "Killstreak de 30 ou plus:");
		player.sendMessage(ChatColor.GOLD + "- Tout les effets précèdents durant 60 secondes");
		player.sendMessage(ChatColor.GRAY + "--------------------------------");
		
	}

}
