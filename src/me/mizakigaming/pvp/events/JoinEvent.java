package me.mizakigaming.pvp.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.mizakigaming.main.PvpMain;

public class JoinEvent implements Listener
{

	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();

		if (PvpMain.getInstance().getCommand().isStart() == true)
		{

			PvpMain.getInstance().getConfig().set(player.getName(), 0);
			PvpMain.getInstance().saveConfig();

		}
		else
			return;
	}
}
