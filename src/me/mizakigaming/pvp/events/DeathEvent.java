package me.mizakigaming.pvp.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import me.mizakigaming.main.PvpMain;
import net.md_5.bungee.api.ChatColor;

public class DeathEvent implements Listener
{

	private final static int	TROIS	= 3;
	private final static int	CINQ	= 5;
	private final static int	SEPT	= 7;
	private final static int	DIX		= 10;
	private final static int	QUINZE	= 15;
	private final static int	VINGT	= 20;

	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
		Player victim = e.getEntity().getPlayer(), killer = e.getEntity().getKiller();

		int numberVictim = PvpMain.getInstance().getConfig().getInt(victim.getName());
		PvpMain.getInstance().getConfig().set(victim.getName(), 0);

		if (killer != null)
			PvpMain.getInstance().getConfig().set(killer.getName(),
					PvpMain.getInstance().getConfig().getInt(killer.getName()) + 1);

		int numberKiller = PvpMain.getInstance().getConfig().getInt(killer.getName());

		PvpMain.getInstance().saveConfig();

		killer.setHealth(killer.getHealth()+4.0);
		
		if (PvpMain.getInstance().getCommand().isStart() == true)
		{
			switch (numberKiller)
			{
				case TROIS:
					this.trois(killer);
					break;
				case CINQ:
					this.cinq(killer);
					break;
				case SEPT:
					this.sept(killer);
					break;
				case DIX:
					this.dix(killer);
					break;
				case QUINZE:
					this.quinze(killer);
					break;
				case VINGT:
					this.vingt(killer);
					break;
			}

			if (numberKiller >= 30)
			{
				this.more(killer, numberKiller);
			}

			if (numberVictim >= 3)
			{
				Bukkit.broadcastMessage(ChatColor.YELLOW + victim.getName() + ChatColor.WHITE
						+ " est mort en killstreak de " + ChatColor.GOLD + numberVictim);
			}

			if (numberVictim > PvpMain.getInstance().getConfig().getInt("BestScore.Score"))
			{
				PvpMain.getInstance().getConfig().set("BestScore.Score", numberVictim);
				PvpMain.getInstance().getConfig().set("BestScore.Joueur", victim.getName());
				PvpMain.getInstance().saveConfig();
				Bukkit.broadcastMessage(ChatColor.GREEN + "Nouveau record de killstreak ! " + ChatColor.WHITE
						+ "il est maintenant d�tenu par " + ChatColor.GOLD + victim.getName() + ChatColor.WHITE
						+ " avec un killstreak de " + ChatColor.GOLD + numberVictim);
				Bukkit.broadcastMessage(ChatColor.GOLD+"/score "+ChatColor.WHITE+"pour voir le meilleur killstreak");
			}

		}
		else
			return;

	}

	public void trois(Player player)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 20, 0), true);
		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "3");
	}

	public void cinq(Player player)
	{
		player.setHealth(player.getMaxHealth());
		player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 20, 0), true);
		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "5");
	}

	public void sept(Player player)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 40, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 40, 1), true);
		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "7");
	}

	public void dix(Player player)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 20 * 25, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 25, 1), true);
		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "10");
	}

	public void quinze(Player player)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 20 * 40, 1), true);

		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "15");
	}

	public void vingt(Player player)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 120, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 120, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 20 * 60, 1), true);
		Bukkit.broadcastMessage(ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de "
				+ ChatColor.GOLD + "20");
	}

	public void more(Player player, int n)
	{
		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 60, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 20 * 60, 1), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 60, 2), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 60, 2), true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 20 * 120, 2), true);

		Bukkit.broadcastMessage(
				ChatColor.YELLOW + player.getName() + ChatColor.WHITE + " est en killstreak de " + ChatColor.GOLD + n);
	}
}
